# FastAPI looks pretty good

Source: https://realpython.com/fastapi-python-web-apis/

Related: https://realpython.com/courses/python-rest-apis-with-fastapi/

```bash
python -V
Python 3.11.4

python -m venv venv

python -m pip install fastapi "uvicorn[standard]"

uvicorn main:app --reload

# test in browser -> http://127.0.0.1:8000/docs
```