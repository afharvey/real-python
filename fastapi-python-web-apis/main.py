from typing import Optional, List

from fastapi import FastAPI
from pydantic import BaseModel


class Item(BaseModel):
    id: Optional[int] = None
    name: str
    url: str
    description: Optional[str] = None
    labels: Optional[List[str]] = None


app = FastAPI()


@app.get("/")
@app.get("/demo")
async def root():
    return {"message": "Imapwnu of Azeroth"}


@app.get("/users/me")
async def read_user_me():
    return {"user_id": "the current user"}


@app.get("/users/{user_id}")
async def read_user(user_id: str):
    return {"user_id": user_id}


@app.get("/items/{item_id}")
async def read_item(item_id: int):
    return {"message": f"you asked for item: #{item_id}"}


@app.post("/items/{item_id}")
async def create_item(item_id: int, item: Item):
    item.id = item_id
    return item
